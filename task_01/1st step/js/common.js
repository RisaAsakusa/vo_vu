

    $(document).ready(function () {


        // hide #back-top first
        // $("#back-top").hide();

        // fade in #back-top
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });

            // scroll body to 0px on click
            $('#back-top').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });


    });
    
    var im = document.getElementById('button_menu');
    function changeImage() {
        if (im.className == 'image_button_menu') {
            $('#button_menu').removeClass('image_button_menu').addClass('image_button_menu_a');

        } else {
            $('#button_menu').removeClass('image_button_menu_a').addClass('image_button_menu');
        }


    };