
$( document ).ready(function() {

includeHTML();
function includeHTML() {
    var z, i, elmnt, file, xhttp;
    /*loop through a collection of all HTML elements:*/
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
      elmnt = z[i];
      /*search for elements with a certain atrribute:*/
      file = elmnt.getAttribute("include-html");
      if (file) {
        /*make an HTTP request using the attribute value as the file name:*/
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4) {
            if (this.status == 200) {elmnt.innerHTML = this.responseText;}
            if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
            /*remove the attribute, and call this function once more:*/
            elmnt.removeAttribute("include-html");
            includeHTML();
            setMENU();
          }
        } 
        xhttp.open("GET", file, true);
        xhttp.send();
        /*exit the function:*/
       
        return;
      }
    }
  }
  function setMENU(){
      var elmnt_, z_, i_, file_;
      z_ = document.getElementsByTagName("*");
      for (i_ = 0; i_ < z_.length; i_++){
       elmnt_ = z_[i_];
       file_ = elmnt_.getAttribute("menu-action");
       if (file_){
         var action_ = document.getElementById(file_);
         action_.classList.add("menu_active");
         //console.log(action_);
         elmnt_.removeAttribute("menu-action");
         includeHTML();
       }
      }
   }
});