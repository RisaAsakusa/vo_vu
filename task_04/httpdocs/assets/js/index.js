//width of file design = 640;
const width_size = 640;
var is_first_load = true;
var arr_fontSize = [];
function click_button(b_id) {
    var icon_ = document.getElementById("icon-row-" + b_id);
    var feetable_ = document.getElementById("fee-table-" + b_id);
    var classname = document.getElementById("icon-row-" + b_id).getAttribute("class");
    if (feetable_ != null) {
        var feetable = document.getElementById("fee-table-" + b_id).getAttribute("class");
        if ($("#icon-row-" + b_id).hasClass("hide-colection")) {
            icon_.classList.remove("hide-colection");

            if (!$("#fee-table-" + b_id).hasClass("hide-table")) {
                feetable_.classList.add("hide-table");
            }
        } else {
            icon_.classList.add("hide-colection");
            if ($("#fee-table-" + b_id).hasClass("hide-table")) {
                feetable_.classList.remove("hide-table");
            }
        }
    }
}
flexFont = function () {
    var divs = document.getElementsByClassName("flexFont");
    if ($(window).width() <= width_size) {
        if (is_first_load) {
            for (var i = 0; i < divs.length; i++) {
                let style = window.getComputedStyle(divs[i], null).getPropertyValue('font-size');
                arr_fontSize[i] = parseFloat(style);
            }
            is_first_load = false;
        }
        for (var i = 0; i < divs.length; i++) {
            var style = window.getComputedStyle(divs[i], null).getPropertyValue('font-size');
            divs[i].style.fontSize = ((((($(window).width() * 100) / width_size) * arr_fontSize[i]) / 100) / 10) + 'rem';
        }
    }
};
window.onload = function (event) {
    flexFont();
    var display_ = document.getElementById("event_");
    display_.style.display = "block";
};

window.onresize = function (event) {
    flexFont();
};




